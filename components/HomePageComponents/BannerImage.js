import React from 'react'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Image from 'react-bootstrap/Image'
import Link from 'next/link'
import styles from '../../styles/GoldAndSilver.module.scss'
import { Slide } from 'react-slideshow-image'
import 'react-slideshow-image/dist/styles.css'

const slideImages = [
  '/Mask_Group_24.png',
  '/Mask_Group_42.png',
  '/Mask_Group_24.png'
];

const Slideshow = () => {
    return (
      <div className="slide-container">
        <Slide>
          <div className="each-slide">
           <Image fluid className={styles.image} src="/Mask_Group_24.png" />
          </div>
          <div className="each-slide">
            <Image fluid className={styles.image} src="/Mask_Group_24.png" />
          </div>
        </Slide>
      </div>
    )
}


function OverflowMainHeadindInfo() {
    return(
        <div className={styles.textOverflowWrapper}>
            <p className={styles.ChampagneSavartAllocation}>Champagne Savart New Release Allocation
            <p className={styles.TastingWithFrederiRarities}>Tasting with Frederic Savart in his Domaine in the little hamlet of Ecueil is nothing short of breathtaking. WEA always start with tasting the vin clairs and then the new releases (and some rarities).</p>
            <button className={styles.home_button}><p className={styles.Explore}>Explore</p><Image fluid className={styles.arrow_back_black_24dp} src="/arrow_back_black_24dp.png" /></button></p>
        </div>
    ); 
}

function GoldBlock() {
    return(
        <div className={styles.imageContainer}>
            <Link href="/news" passHref>
                <a>
                  <Slideshow></Slideshow>
                  <OverflowMainHeadindInfo></OverflowMainHeadindInfo>
                </a>
            </Link>
        </div>
    );
}


class BannerImage extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return( 
            <Container fluid className={styles.customContainerBanner}>
                <Row>
                    <Col lg={12}>
                        <GoldBlock></GoldBlock>
                    </Col>
                </Row>
            </Container>
        ); 
    }
}

export default BannerImage
