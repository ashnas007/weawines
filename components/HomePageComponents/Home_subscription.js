import React from 'react'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Image from 'react-bootstrap/Image'
import Link from 'next/link'
import styles from '../../styles/GoldAndSilver.module.scss'


function subscription() {
    return(
        <div className={styles.WEA_from_Home_Subscription}>
            <p>WEA from Home Subscription</p>
        </div>
    );
}

class Home_subscription extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return( 
            <Container fluid className={styles.customContainerBanner}>
                <Row>
                    <Col lg={12}>
                        <div className={styles.WEA_from_Home_Subscription}>
                            <p>WEA from Home Subscription</p>
                        </div>
                    </Col>
                </Row>
                <Row>
                    <Col lg={6}>
                        <div>
                            <Image className={styles.Image_16} src="/home_images/wine_subsciption.png" />
                        </div>
                    </Col>
                    <Col lg={6}>
                        <div className={styles.WEA_Wines_ourselves_sub1} style={{marginTop:"50px",marginLeft:"40px"}}>
                            <p>-- A wine subscription plan like no others.</p>
                            <p>-- Choose from <span className={styles.STARTER_ADVANCED}>STARTER</span> or <span className={styles.STARTER_ADVANCED}>ADVANCED </span>tier plans.</p>
                            <p>-- Start with a 3 month subscription OR if you are ready for the challenge – 6 months for more
   benefits!</p>
                            <p>-- And what is it NOT? It isn’t a scheme to pull in as many subscribers as possible. Strictly not more
  than 24 members in each exclusive tier.</p>
                            <Row>
                              <button className={styles.Rectangle_12} style={{margin:"3px 1px 14px 14px", width:"230px"}}><span className={styles.Subscribe}>STARTER TIER Subscription</span></button>
                              <button className={styles.Rectangle_12} style={{margin:"3px 1px 14px 14px", width:"230px"}}><span className={styles.Subscribe}>Advanced TIER Subscription</span></button>
                              </Row>
                        </div>
                    </Col>
                </Row>
                <Row>
                <Col lg={12}>
                    <div className={styles.Rectangle_128}>
                    <p className={styles.Events}>Events</p>
                    <Row>
                    <Col lg={3}>
                        <Image  className={styles.Mask_Group_26} src="/home_images/Mask_Group_26.png" />
                    </Col>
                    <Col lg={3}>
                        <Image className={styles.Mask_Group_26} style={{marginLeft:"35px"}} src="/home_images/Mask_Group_27.png" />
                    </Col>
                    <Col lg={3}>
                        <Image className={styles.Mask_Group_26} style={{marginLeft:"70px"}} src="/home_images/Mask_Group_28.png" />
                    </Col>
                    </Row>
                    </div>
                </Col>
                </Row>
            </Container>
        ); 
    }
}

export default Home_subscription
