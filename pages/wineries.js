import React from 'react'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Link from 'next/link'
import Container from 'react-bootstrap/Container'
import styles from '../styles/GoldAndSilver.module.scss'
import Image from 'react-bootstrap/Image'

export default function Wineries() {
    return(
        <>
           <Container fluid className={styles.customContainerBanner}>
                <Row>
                    <Col lg={12}>
                        <div className={styles.imageContainer}>
                            <Link href="#" passHref>
                                <a>
                                  <Image fluid className={styles.image} src="/wineries.png" />
                                  <div className={styles.textOverflowWrapper}>
                                      <p className={styles.ChampagneSavartAllocation}>WINERIES</p>
                                      
                                  </div>
                                </a>
                            </Link>
                        </div>
                    </Col>
                </Row>
                <Row>
                    <Col lg={12}>
                        <div className={styles.imageContainer}>
                            <Link href="#" passHref>
                                <a>
                                  <Image fluid className={styles.image} src="/wineries/wineries1.png" />
                                  <div className={styles.textOverflowWrapper}>
                                      <p className={styles.ChampagneSavartAllocation}>BURGUNDY</p>
                                      
                                  </div>
                                </a>
                            </Link>
                        </div>
                    </Col>
                </Row>
            </Container>
        </>
    );
}