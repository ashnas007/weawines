import React from 'react'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Link from 'next/link'
import Container from 'react-bootstrap/Container'
import styles from '../styles/GoldAndSilver.module.scss'
import Image from 'react-bootstrap/Image'

export default function Contact_us() {
    return(
        <>
           <Container fluid className={styles.customContainerBanner}>
                <Row>
                    <Col lg={12}>
                        <div className={styles.imageContainer}>
                            <Link href="#" passHref>
                                <a>
                                  <Image fluid className={styles.image} src="/contact_us.png" />
                                  <div className={styles.textOverflowWrapper}>
                                      <p className={styles.ChampagneSavartAllocation}>CONTACT US</p>
                                      
                                  </div>
                                </a>
                            </Link>
                        </div>
                    </Col>
                </Row>
                <Row>
                    <Col lg={6}>
            <p className={styles.Lets_Connect} style={{ margin: "60px 419.4px 3px 150px"}}>
            Let's Connect
            </p>
            <input type="text" name="fullname" className={styles.Rectangle_205} style={{margin: "34px 42.4px 19px 150px", padding: "12px 100px 11px 20px", width: "437px"}} placeholder="Fullname" />
            <input type="text" name="fullname" className={styles.Rectangle_205} style={{margin: "19px 42.4px 19px 150px", padding: "12px 100px 11px 20px", width: "437px"}} placeholder="Email" />
            <input type="text" name="fullname" className={styles.Rectangle_205} style={{margin: "19px 42.4px 19px 150px", padding: "12px 100px 11px 20px", width: "437px"}} placeholder="Contact No" />
            <textarea className={styles.Rectangle_209} name="comments" placeholder="Tell us your comments"></textarea>
            <button className={styles.Rectangle_12}><span className={styles.Subscribe}>SEND</span></button>
                    </Col>
                    <Col lg={3}>
                    <Row style={{marginTop: "100px"}}>
                        <Image fluid className={styles.placeholder_loc} src="/location.png" /><p className={styles.telephone} style={{margin: "40.7px 8px 23.3px 12.3px", width: "214px", height: "42px"}}>Blk 341,Le Bon Funk, 29 Club St
            Singapore 069414
                        </p>
                        </Row>
                        <Row >
                        <Image fluid className={styles.telephone} src="/telephone.png" /><p className={styles.telephone} style={{margin: "36px 14px 32px 12.3px", width: "153px", height: "23px"}}>info@weawines.com.sg
                        </p>
                        </Row>
                                </Col>
                    <Col lg={3}>
                    <Row style={{marginTop: "110px"}}>
                        <Image fluid className={styles.mail} src="/mail_icon.png" /><p className={styles.mail}>+65 7865 7679
                        </p>
                        </Row>
                    </Col>
                </Row>            </Container>
        </>
    );
}