import React from 'react'
import Container from 'react-bootstrap/Container'
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import { CartButton } from '../UtilsComponents/Buttons'
import { CartCount } from '../UtilsComponents/Buttons'
import Image from 'react-bootstrap/Image'
import styles from '../../styles/GoldAndSilver.module.scss'

export const linksCategories = [
    { 
        name: 'Wineries',
        path: '/rings',
        image: '../CategoriesThumbnails/RingsThumbnail.jpg'
    },
    {
        name: 'Price List',
        path: '/necklaces',
        image: '../CategoriesThumbnails/NecklacesThumbnail.jpg'
    },
    {
        name: 'News',
        path: '/earrings',
        image: '../CategoriesThumbnails/EarringsThumbnail.jpg'
    },
    {
        name: 'Mailing List',
        path: '/watches',
        image: '../CategoriesThumbnails/WatchesThumbnail.jpg'
    },
    {
        name: 'About',
        path: '/about_us',
        image: '../CategoriesThumbnails/EarringsThumbnail.jpg'
    },
    {
        name: 'Contact',
        path: '/earrings',
        image: '../CategoriesThumbnails/EarringsThumbnail.jpg'
    }
]

const ToggleStyle = {
    backgroundImage: "url('../public/Toogle.svg)"
}

export default function NavbarComponent() {
    return(
        <Container style={{maxWidth: '100%'}}>
        <div style={{backgroundColor: "#a61103", boxShadow: "none", textAlign: "left", paddingLeft: "10%"}} collapseOnSelect expand="lg">
            <Navbar.Toggle style={{background: 'transparent', color: "#fff", border: "none"}} aria-controls="responsive-navbar-nav" />
            <Navbar.Brand style={{color: "#fff", fontSize: ".80rem"}} href="/">
                Free delivery for $300 and above, till 13th June 2021 only  
            </Navbar.Brand>
             <Navbar.Brand style={{color: "#fff", fontSize: ".80rem", paddingLeft: "35%"}} href="/">
                <i class="bi bi-person-fill"></i> Register/Login &nbsp;&nbsp;&nbsp;|
            </Navbar.Brand>
             <Navbar.Brand style={{color: "#fff", fontSize: ".80rem"}} href="/">
                <i class="bi bi-bag-check"></i> Cart Items (<CartCount />) 
            </Navbar.Brand>
        </div>
        <div style={{backgroundColor: "transparent", boxShadow: "none", textAlign: "center", marginTop: "20px"}} collapseOnSelect expand="lg">
            <Navbar.Toggle style={{background: 'transparent', color: "#000", border: "none"}} aria-controls="responsive-navbar-nav" />
            <Navbar.Brand style={{color: "#000", fontSize: "1.74rem"}} href="/">
                <Image fluid  src="/wea-logo.png" />
            </Navbar.Brand>
        </div>
        <nav class="navbar navbar-expand-lg navbar-dark" style={{background: '#ffffff', border: "solid 1px #e6e6e6", padding: "12px 20px 11px 30%"}}>
        <div class="container-fluid">
          <div class="collapse navbar-collapse" id="navbarColor01">
            <ul class="navbar-nav me-auto">
              <li class="nav-item">
                <a className={styles.WineriesPriceList} href="/wineries">Wineries
                </a>
              </li>
              <li class="nav-item">
                <a className={styles.WineriesPriceList} href="#">Price List</a>
              </li>
              <li class="nav-item">
                <a className={styles.WineriesPriceList} href="/news">News</a>
              </li>
              <li class="nav-item">
                <a className={styles.WineriesPriceList} href="/mail_list">Mailing List</a>
              </li>
               <li class="nav-item">
                <a className={styles.WineriesPriceList} href="/about">About</a>
              </li>
               <li class="nav-item">
                <a className={styles.WineriesPriceList} href="/contact_us">Contact</a>
              </li>
            </ul>
            <form class="d-flex">
              <input class="form-control me-sm-2" type="text" style={{background: '#ffffff', border: "none", padding: "0px 0px 0px 15%",textAlign: "right"}} placeholder="Search..." />
              <Image className={styles.image} style={{width:"20px",height:"20px",marginTop:"11px",marginLeft:"13px"}} src="/search.png" />
            </form>
          </div>
        </div>
      </nav>
        </Container>
    )
}