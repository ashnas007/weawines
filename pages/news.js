import React from 'react'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Link from 'next/link'
import Container from 'react-bootstrap/Container'
import styles from '../styles/GoldAndSilver.module.scss'
import Image from 'react-bootstrap/Image'

const containerStyle = {
    boxShadow: "0 50px 100px -20px rgb(50 50 93 / 25%), 0 30px 60px -30px rgb(0 0 0 / 30%)",
    headerFont: "26px 'Unna', serif",
    contentFont: "18px 'Unna', serif",
    borderTop: "3px ridge rgba(0, 0, 0, 0.25)",
    textAlign: "center",
    margin: "100px 50px"
}

export default function About() {
    return(
        <>
           <Container fluid className={styles.customContainerBanner}>
                <Row>
                    <Col lg={12}>
                        <div className={styles.imageContainer}>
                            <Link href="/elements/gold" passHref>
                                <a>
                                  <Image fluid className={styles.image} src="/Mask_Group_42.png" />
                                  <div className={styles.textOverflowWrapper}>
                                      <p className={styles.ChampagneSavartAllocation}>ABOUT</p>
                                      
                                  </div>
                                </a>
                            </Link>
                        </div>
                    </Col>
                </Row>
            </Container>
            <div>
            <p className={styles.Lorem_ipsum_dolor_labore}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur
            </p></div>
        </>
    );
}