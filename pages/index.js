import React from 'react'
import FirstSection from '../components/HomePageComponents/FirstSection'
import Home_subscription from '../components/HomePageComponents/Home_subscription'
import Home_news from '../components/HomePageComponents/Home_news'
import TrendingSection from '../components/HomePageComponents/TrendingSection'
import GoldAndSilverSection from '../components/HomePageComponents/GoldAndSilverSection'
import BannerImage from '../components/HomePageComponents/BannerImage'
import CustomersReviewsSection from '../components/HomePageComponents/CustomersReviewsSection'
import ShipmentsInformation from '../components/HomePageComponents/ShipmentsInformation'
import { wrapper } from '../store' 
import { filterProducts } from '../store/actions/productsActions'

export const getServerSideProps = wrapper.getServerSideProps(async ({store}) => {
  await store.dispatch(filterProducts('state', 'trending'))
  return {
    props: {
      trendingProducts: store.getState().productsReducer.server.filteredProducts
    }
  }
}) 

export function Home({trendingProducts}) {
  return (
    <>
      <BannerImage></BannerImage>
      <Home_subscription></Home_subscription>
      <TrendingSection trendingProducts={trendingProducts}></TrendingSection>
      <Home_news></Home_news>
      <GoldAndSilverSection></GoldAndSilverSection>
      <CustomersReviewsSection></CustomersReviewsSection>
      <ShipmentsInformation></ShipmentsInformation>
    </>
  )
}

export default Home;

