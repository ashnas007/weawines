import React from 'react'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Link from 'next/link'
import Container from 'react-bootstrap/Container'
import styles from '../styles/GoldAndSilver.module.scss'
import Image from 'react-bootstrap/Image'

export default function Mail_list() {
    return(
        <>
           <Container fluid className={styles.customContainerBanner}>
                <Row>
                    <Col lg={12}>
                        <div className={styles.imageContainer}>
                            <Link href="#" passHref>
                                <a>
                                  <Image fluid className={styles.image} src="/mail.png" />
                                  <div className={styles.textOverflowWrapper}>
                                      <p className={styles.ChampagneSavartAllocation}>MAILING LIST</p>
                                      
                                  </div>
                                </a>
                            </Link>
                        </div>
                    </Col>
                </Row>
            </Container>
            <div className={styles.Rectangle_204}>
            <p className={styles.Join_our_Mailing_List}>JOIN OUR MAILING LIST</p>
            <p className={styles.Signup_to_receive}>Signup to receive email updates on New Product Announcements, Gift Ideas,
Special Promotions, Sales and more...</p>
            <input type="text" name="fullname" className={styles.Rectangle_205} placeholder="Fullname" />
            <input type="text" name="email" className={styles.Rectang_206} placeholder="Email" />
            <button className={styles.Rectangle_12}><span className={styles.Subscribe}>SUBSCRIBE</span></button>
            <p className={styles.Terms_and_Privacy_Policy}>By clicking "SUBSCRIBE", you agree to WEA wines's Terms of Use and Privacy Policy</p>
            </div>
        </>
    );
}