import React from 'react'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Link from 'next/link'
import Container from 'react-bootstrap/Container'
import styles from '../styles/GoldAndSilver.module.scss'
import Image from 'react-bootstrap/Image'

const containerStyle = {
    boxShadow: "0 50px 100px -20px rgb(50 50 93 / 25%), 0 30px 60px -30px rgb(0 0 0 / 30%)",
    headerFont: "26px 'Unna', serif",
    contentFont: "18px 'Unna', serif",
    borderTop: "3px ridge rgba(0, 0, 0, 0.25)",
    textAlign: "center",
    margin: "100px 50px"
}

export default function Wineries_individuals() {
    return(
        <>
           <Container fluid className={styles.customContainerBanner}>
                <Row>
                    <Col lg={12}>
                        <div className={styles.imageContainer}>
                            <Link href="#" passHref>
                                <a>
                                  <Image fluid className={styles.image} src="/wineries_individual.png" />
                                  <div className={styles.textOverflowWrapper}>
                                      <p className={styles.ChampagneSavartAllocation}>Antoine Jobard</p>
                                      
                                  </div>
                                </a>
                            </Link>
                        </div>
                    </Col>
                </Row>
            </Container>
            <Container fluid className={styles.About}>
            <Row>
                    <Col lg={6}>
                        <div>
                            <Image className={styles.Image_16} src="/CustomersImages/Image_16.png" />
                        </div>
                    </Col>
                    <Col lg={5}>
                      <div >
                        <p><h5 style={{margin: "20px 0px 3px"}}>Antoine Jobard</h5></p>
                        <p><h6>What WEA say?</h6></p>
                        <p className={styles.WEA_Wines_ourselves_sub1} style={{margin: "20px 0px 3px"}} >
                                   Antoine – the 5th generation of the family – officially took over the Domaine from his father Francois in 2006. They have just over 6 ha of vines predominantly in 
                                   Meursault and they only make white wines. Very little new oak is used here – 
                                   only 15% on average. The Bourgogne Blanc is usually a sign of how great a 
                                   Domaine is and the example here, made from 4 different parcels around Meursault, 
                                   is absolutely stunning and a ringer for a basic villages Meursault. 
                        </p>
                        <p className={styles.WEA_Wines_ourselves_sub1} style={{margin: "20px 0px 3px", height: "100px"}}>
                        Antoine makes very pure and understated Meursaults. In my opinion, the style is more Roulot than Lafon. The main market for their wines is inside Europe itself and only bits make it to the UK and USA. Domaine Antoine Jobard is one of the few Domaines which I’ve been following for a while and it gives me immense pleasure to represent them in Singapore.</p>
                      </div>
                    </Col>
                </Row>
                <Row>
                    <Col lg={12}>
                    <div className={styles.Lorem_ipsum_dolor_labore}>

                    <p className={styles.Small_headings}>What do the Critics say?</p>
                    <p className={styles.Small_heading1}>Allen Meadows, Burghound #55?</p>
            <p>
            I cannot really emphasize strongly enough just how difficult it was in Meursault in this vintage, as I asked Monsieur Jobard how much worse the production was in 2012 versus the very short crop of 2010 and he replied “we are down anywhere from twenty to one hundred percent- depending on the vineyard- from our low yields of 2010.” The only good news here is the remaining wines are absolutely brilliant and about as beautiful a set of wines as I have ever tasted from Domaine Jobard, and given that I rank this estate right up in my top five or six white Burgundy producers from anywhere, that is pretty high praise for this gorgeous range of wines. I found none of the meta-concentration here that is found in some Côte de Beaune whites in 2012, just magically pure, deep and crunchy white Burgundies of stunning balance and transparency. The family also has a new wine in their lineup in this vintage, a fine premier cru bottling of St. Aubin from the vineyard of “Sur le Sentier du Clou”, which lies mid-slope in the band of premier crus that start up behind the village center with Derrière Chez Edouard and extend towards the border with Puligny. In a typical vintage there should be five barrels of this fine newcomer to the Jobard lineup, but in 2012, there are only two barrels of this exciting new cuvée for the family. The Jobards purchased this quarter hectare parcel of twenty-one year-old vines in 2012 and this is going to earn fans very, very quickly and is just superb in its inaugural vintage. In fact, there is not one wine in the range of 2012 Jobards that I would not want in my cellar in serious quantity if the possibility existed, and do not overlook even the superb Bourgogne blanc this year if that is all that is offered to you from your Jobard merchant, as every single wine that will emerge from 2012 chez Jobard is about as beautiful an example of its respective appellation as one could dream of finding.
            
            </p>
            <p className={styles.Small_heading1} style={{marginTop : "3%"}}>John Gilman, View from the Cellar #48</p>
             <p>
             Given the staggering hail damage in 2012 in Meursault, I really did not expect to see any of my favorite producers in this village on my November trip, as I just expected most estates to have no interest in seeing journalists with so little wine left after the brutal summer of 2012 here. Much to my surprise, Antoine Jobard was happy to receive me and show me a truly stunning range of 2012s- albeit in tiny quantities. I cannot really emphasize strongly enough just how difficult it was in Meursault in this vintage, as I asked Monsieur Jobard how much worse the production was in 2012 versus the very short crop of 2010 and he replied “we are down anywhere from twenty to one hundred percent- depending on the vineyard- from our low yields of 2010.” The only good news here is the remaining wines are absolutely brilliant and about as beautiful a set of wines as I have ever tasted from Domaine Jobard, and given that I rank this estate right up in my top five or six white Burgundy producers from anywhere, that is pretty high praise for this gorgeous range of wines. I found none of the meta-concentration here that is found in some Côte de Beaune whites in 2012, just magically pure, deep and crunchy white Burgundies of stunning balance and transparency. The family also has a new wine in their lineup in this vintage, a fine premier cru bottling of St. Aubin from the vineyard of “Sur le Sentier du Clou”, which lies mid-slope in the band of premier crus that start up behind the village center with Derrière Chez Edouard and extend towards the border with Puligny. In a typical vintage there should be five barrels of this fine newcomer to the Jobard lineup, but in 2012, there are only two barrels of this exciting new cuvée for the family. The Jobards purchased this quarter hectare parcel of twenty-one year-old vines in 2012 and this is going to earn fans very, very quickly and is just superb in its inaugural vintage. In fact, there is not one wine in the range of 2012 Jobards that I would not want in my cellar in serious quantity if the possibility existed, and do not overlook even the superb Bourgogne blanc this year if that is all that is offered to you from your Jobard merchant, as every single wine that will emerge from 2012 chez Jobard is about as beautiful an example of its respective appellation as one could dream of finding.
            </p></div>
                    </Col>
                </Row>
            </Container>
        </>
    );
}