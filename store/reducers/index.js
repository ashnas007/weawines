import { combineReducers } from 'redux'
import productsReducer from './productsReducer'

const reducers = combineReducers({
    productsReducer,
});

export default reducers;