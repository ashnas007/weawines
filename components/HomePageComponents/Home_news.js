import React from 'react'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Image from 'react-bootstrap/Image'
import Link from 'next/link'
import styles from '../../styles/GoldAndSilver.module.scss'


function news() {
    return(
        <div className={styles.WEA_from_Home_Subscription}>
            <p>News</p>
        </div>
    );
}

class Home_news extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return( 
            <Container fluid className={styles.customContainerBanner}>
                <Row>
                    <Col lg={12}>
                        <div className={styles.WEA_from_Home_Subscription}>
                            <p>News</p>
                        </div>
                    </Col>
                </Row>
                <Row>
                    <Col lg={5}>
                        <div>
                           <Image  className={styles.Mask_Group_101} src="/home_images/Mask_Group_101.png" />
                        </div>
                    </Col>
                    <Col lg={6}>
                        <div className={styles.WEA_Wines_ourselves_sub1} style={{marginTop:"50px"}}>
                            <p className={styles.Champagne_Allocation}>Champagne Savart New Release Allocation</p>
                            <p>Tasting with Frederic Savart in his Domaine in the little hamlet of Ecueil is nothing short of breathtaking. WEA always start with tasting the vin clairs and then the new releases (and some rarities).</p>
                            <p>Papa Daniel often makes a guest appearance too.There will be several upcoming cuvees from the new negoce arm which will only see the light of the day in a few year’s time. The duo 2014 vintage cuvees of Le Mont Benoit (finally available in mags) and Le Mont des Chretiens (finally available in bots) are truly exceptional this year, with slightly more quantities to fulfill everyone’s needs. The wines are engaging and vinous, and absolutely charming with the right balance of linearity and generosity. Oh and a 2008 tasted blind was such a delight, showing hints of secondary evolution and complexity. This year’s release will also include some late disgorged jeroboams of 2010 L’Annee (the first jeros I’ve seen from the Domaine) and some late disgorgements of 2013 L’Annee. Oh and the celebrated Expression cuvee finally makes a reappearance in the 2014 vintage.</p>
                            <p>View Details</p>
                        </div>
                    </Col>
                </Row>
            </Container>
        ); 
    }
}

export default Home_news
